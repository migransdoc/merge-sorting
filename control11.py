def smallest(numbers):
    m1, m2 = float('inf'), float('inf')
    for x in numbers:
        if x <= m1:
            m1, m2 = x, m1
        elif x < m2:
            m2 = x
    return tuple([m1, m2])

a = list(map(int, input().split()))
b1 = ['000', '100', '10', '01', '11']
b2 = ['1001', '0001', '101', '11', '0']
c = sorted(a)
d = dict()

first, second = smallest(a)
b = b2 if max(a) > first+second else b1

for ind, val in enumerate(c):
    d[b[ind]] = val
for i in a:
    for j in d:
        if d[j] == i:
            print(j)
            del d[j]
            break
