from math import log2
a = list(map(float, input().split()))
ans = 0
for i in a:
    ans += i * log2(i)
print(abs(ans))
