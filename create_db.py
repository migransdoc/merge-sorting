import sqlite3

#Подключение к базе
con = sqlite3.connect("workers.db")

#Создание курсора
c = con.cursor()




#Создание таблиц
c.execute(
"""
CREATE TABLE Department (
   id int auto_increment primary key,
   name varchar(100)
)
""")

c.execute("""
CREATE TABLE Employee (
   id int auto_increment primary key,
   department_id int,
   chief_id int,
   name varchar(100),
   salary int,
   FOREIGN KEY (department_id) REFERENCES Department(id),
   FOREIGN KEY (chief_id) REFERENCES Employee(id)
)
""")


#Наполнение таблицы
values = [(1, 'робототехники'), (2, 'шаурмичный'), (3, 'котиков'), (4, 'счастья')]
c.executemany("""INSERT INTO Department (id, name) VALUES (?, ?)""", values)


values = [
   (1, 1, 1, "Петя", 1700),
   (2, 2, 1, "Валя", 100),
   (3, 4, 1, "Аня", 400),
   (4, 3, 1, "Соня", 500),
   (5, 2, 2, "Рита", 500),
   (6, 2, 2, "Кеша", 200),
   (7, 3, 4, "Гена", 300),
   (8, 2, 4, "Варя", 900)
]
c.executemany("""INSERT INTO Employee (id, department_id, chief_id, name, salary) VALUES (?, ?, ?, ?, ?)""", values)
#Подтверждение отправки данных в базу
con.commit()


c.execute("""
select * from Department
""")

print(c.fetchall())

c.execute("""
select * from Employee
""")

print(c.fetchall())

#Завершение соединения
c.close()
con.close()
