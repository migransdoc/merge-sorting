import sqlite3
con = sqlite3.connect('workers.db')
c = con.cursor()

c.execute("""
select a.name from Employee a, Employee b
where b.id = a.chief_id and a.salary > b.salary
""")
print(c.fetchall())
print()

c.execute("""
select a.name from Employee a
where a.salary = (select max(salary) from Employee b where a.department_id = b.department_id)
""")
print(c.fetchall())
print()

c.execute("""
select department_id from Employee
group by department_id
having 3 >= count(department_id)
""")
print(c.fetchall())
print()

c.execute("""
select a.name from Employee a, Employee b
where b.id = a.chief_id and a.department_id <> b.department_id
""")
print(c.fetchall())
print()

c.execute("""
select department_id from Employee

where sum_salary in (
select  max(sum_salary) from
(select sum(salary) as sum_salary, department_id from Employee
group by department_id))

""")
print(c.fetchall())
print()

c.close()
con.close()
