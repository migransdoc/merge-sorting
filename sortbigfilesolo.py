# ONE THREAD SORT
from random import choice
import os

NUMBERS = 12345
RANG = 100000
N = 10
M = 1235

class MergeFile:
    def __init__(self, numbers, rang, n, m):
        self.NUMBERS = numbers # number of elements in input file
        self.RANG = rang
        self.N = n # number of files
        self.M = m # number of elements in file
        self.input = open('input.txt', 'w+')
        self.output = open('output.txt', 'w+')
        self.files = [open('list{}.txt'.format(i), 'w+') for i in range(self.N)]
    
    def generation(self): # generate input file
        for i in range(self.NUMBERS):
            self.input.write(str(choice(list(range(1, self.RANG+1))))+'\n')

    def preparing(self): # adjacent sorted file
        self.input.seek(0) # return cursor at the start of the file
        for i in self.files:
            mass = []
            try:
                for j in range(M):
                    mass.append(int(self.input.readline()))#current mass of elem
            except ValueError: pass
            mass.sort() # sorting
            for u in mass:
                i.write(str(u) + '\n') # write to file

    def merge(self):
        for i in self.files:
            i.seek(0) # return cursor at the start of the file
        current_mas = []
        for i in self.files:
            try:current_mas.append(int(i.readline())) # first elem of every file
            except ValueError:pass
        ind = current_mas.index(min(current_mas))
        while True:
            if len(set(current_mas)) == 1 and 10**9 + 7 in current_mas:
                break # if in list only big numbers
            self.output.write(str(current_mas[ind]) + '\n') # writing min
            try:
                current_mas[ind] = int(self.files[ind].readline())
                # read new elem instead min
            except ValueError: # if no elem in file
                current_mas[ind] = 10**9 + 7 # set big number instead elem from file
            ind = current_mas.index(min(current_mas)) # find index of min

    def deleteAdjacent(self): # delete and close files
        for i in self.files:
            i.close()
        for i in range(self.N):
            os.remove('list{}.txt'.format(i))
        self.input.close()
        self.output.close()

    def checking(self):
        for i in self.files:
            i.seek(0) # return cursor at the start of the file
        self.input.seek(0)
        self.output.seek(0)
        def checkSize(a, b): return len(a.readlines()) == len(b.readlines())
            # check if input and output have same numbers of digits
        def checkSizes(a): return [len(b.readlines()) for b in a]
            # print numbers of digits of adjacent files
        def checkCorrect(a, b): # check sorting
            a = sorted(list(map(int, a.readlines())))
            b = list(map(int, b.readlines()))
            return a == b
        return checkSize(self.input, self.output) and checkCorrect(
            self.input, self.output), checkSizes(self.files)

    def facade(self):
        self.generation()
        self.preparing()
        self.merge()
        print(self.checking())
        self.deleteAdjacent()
        

MergeFile(NUMBERS, RANG, N, M).facade()
